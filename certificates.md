## Helm Installation

```bash
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```  

## Installation of the cert-manager

1. Create the namespace for cert-manager.

```bash
oc create namespace cert-manager
```

Switch to the project cert-manager

```bash
oc project cert-manager
```

2. Add the Jetstack Helm repository.

```bash
helm repo add jetstack https://charts.jetstack.io
```

3. Update your local Helm chart repository cache.

```bash
helm repo update
```

4. To install the cert-manager Helm chart, Run 

```bash
helm install   cert-manager jetstack/cert-manager   --namespace cert-manager   --create-namespace   --version v1.3.1   --set 'extraArgs={--dns01-recursive-nameservers-only,--dns01-recursive-nameservers=8.8.8.8:53\,1.1.1.1:53}' --set installCRDs=true
```

5. Verifying the installation -

```bash
oc get pods --namespace cert-manager
```

Output should be displayed as -

```
NAME                                       READY   STATUS    RESTARTS   AGE
cert-manager-7d5bf7c57c-9zpxm              1/1     Running   0          117m
cert-manager-cainjector-7b744d56fb-5w646   1/1     Running   0          117m
cert-manager-webhook-7d6d4c78bc-5knqz      1/1     Running   0          117m
```

You should see the cert-manager, cert-manager-cainjector, and cert-manager-webhook pod in a Running state. 

## Setting up Issuers

Before you can begin issuing certificates, you must configure at least one Issuer or ClusterIssuer resource in your cluster.

NOTE: Cert-manager supports a number of different issuer backends, each with their own different types of configuration. In our case, ACME issuer backend will be used as Let’s Encrypt certificates will be generated.

1. A secret containing service principal password should be created on Openshift to facilitate presenting the challenge to Azure DNS. You can create the secret with the following command -

```bash
oc create secret generic azuredns-config --from-literal=client-secret=AZURE_SERVICE_PRINCIPAL_PASSWORD
```

We used same azure service principal password which was created for Openshift Installation.

AZURE_SERVICE_PRINCIPAL_NAME = stg-innershift-onprem

2. Create the ClusterIssuer resource -

### Example of clusterissuer.yaml

```yaml
apiVersion: cert-manager.io/v1
kind: ClusterIssuer
metadata:
  name: example-issuer
spec:
  acme:
    email: anyemail
    server: https://acme-v02.api.letsencrypt.org/directory
    privateKeySecretRef:
      name: testing
    solvers:
     - dns01:
        azureDNS:
          clientID: e3b8f433-4f62-492c-a11e-e4df6a92a51c
          clientSecretSecretRef:
           name: azuredns-config
           key: client-secret
          subscriptionID: 9680ffa8-53b4-47c3-9fa0-b01209d296f9
          tenantID: 85173d93-99ef-4dff-9b45-495719659133
          resourceGroupName: stg-innershift-onprem
          hostedZoneName: innershift.ga
          environment: AzurePublicCloud
```

```bash
oc create -f clusterissuer.yaml
```

3. Check that the clusterissuer is generated properly -

```bash

oc get clusterissuer

NAME             READY   AGE
example-issuer   True    130m

```

Note - Ready field should be "true".


## Issuing Certificates

The Certificate resource type is used to request certificates from different Issuers.

1. Create the Certificate resource -

### Example of ingresscertificate.yaml

```yaml
apiVersion: cert-manager.io/v1alpha2
kind: Certificate
metadata:
  name: staging-innershift
  namespace: openshift-ingress
spec:
  secretName: stagingonprem-ingress
  duration: 2160h0m0s # 90d
  renewBefore: 360h0m0s # 15d
  organization:
  - "news"
  keySize: 2048
  keyAlgorithm: rsa
  keyEncoding: pkcs1
  usages:
    - server auth
    - client auth
  # At least one of a DNS Name, URI, or IP address is required.
  dnsNames:
  - '*.apps.stg-onprem.openshiftft.ga'
  issuerRef:
    name: example-issuer
    kind: ClusterIssuer
```

### Example of apicertificate.yaml

```yaml
apiVersion: cert-manager.io/v1alpha2
kind: Certificate
metadata:
  name: staging-innershift
  namespace: openshift-config
spec:
  secretName: stagingonprem-innershift-api
  duration: 2160h0m0s # 90d
  renewBefore: 360h0m0s # 15d
  organization:
  - "sopra"
  keySize: 2048
  keyAlgorithm: rsa
  keyEncoding: pkcs1
  usages:
    - server auth
    - client auth
  # At least one of a DNS Name, URI, or IP address is required.
  dnsNames:
  - 'api.stg-onprem.innershift.ga'
  issuerRef:
    name: example-issuer
    kind: ClusterIssuer
```

oc create -f ingresscertificate.yaml

oc create -f apicertificate.yaml

2. Check that the certificates are generated properly -

```bash
oc get certificate -n openshift-ingress

NAME                 READY   SECRET                     AGE
staging-innershift   True    stagingonprem-innershift-ingress   135m

oc get certificate -n openshift-config
NAME                 READY   SECRET                     AGE
staging-innershift   True    stagingonprem-innershift-api   5m20s

```

Note - Ready field must be "True"


## Patching the OpenShift Routers with the new certificates -

```bash
oc patch ingresscontroller.operator default --type=merge -p '{"spec":{"defaultCertificate": {"name": "stagingonprem-innershift-ingress"}}}' -n openshift-ingress-operator
```

Wait for the router pods to be recreated and in running state -

```bash
watch oc get po -n openshift-ingress
```

Test the certificate -

```bash
echo | openssl s_client -connect console-openshift-console.apps.stg-onprem.innershift.ga:443 -servername console-openshift-console.apps.stg-onprem.innershift.ga
```
It must return a Verify return code: 0 (ok)

## Patching API certificates

```bash
oc patch apiserver cluster  --type=merge -p '{"spec":{"servingCerts": {"namedCertificates":[{"names": ["api.stg-onprem.innershift.ga"],"servingCertificate": {"name": "stagingonprem-innershift-api"}}]}}}'
```

Test the certificate -

```bash
echo | openssl s_client -connect api.stg-onprem.innershift.ga:6443 -servername api.stg-onprem.innershift.ga
```
It must return a Verify return code: 0 (ok)


## External Links -

https://rcarrata.com/openshift/ocp4_renew_automatically_certificates/

https://cert-manager.io/docs/configuration/acme/dns01/azuredns/#service-principal













