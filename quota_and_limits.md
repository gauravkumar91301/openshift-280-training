# Resource Quota and Limit Range

## Create a bootstrap project template

```bash
oc adm create-bootstrap-project-template -o yaml > template.yaml
```

## Complete the template

Add the following lines at the end:

```yaml
- apiVersion: v1
  kind: ResourceQuota
  metadata:
      name: core-resource-quota
  spec:
    hard:
      limits.cpu: "4"
      limits.memory: 10Gi
      persistentvolumeclaims: "32"
      pods: "40"
      requests.storage: 64Gi
- apiVersion: v1
  kind: LimitRange
  metadata:
    name: core-resource-limits
  spec:
    limits:
    - max:
        cpu: "4"
        memory: 4Gi
      min:
        cpu: 10m
        memory: 3Mi
      type: Pod
    - default:
        cpu: 100m
        memory: 250Mi
      defaultRequest:
        cpu: 10m
        memory: 10Mi
      max:
        cpu: "4"
        memory: 4Gi
      min:
        cpu: 10m
        memory: 3Mi
      type: Container
```

## Apply the default template

```bash
oc create -f template.yaml -n openshift-config
```

_or replace it if it already exists_
```bash
oc replace -f project-request.yaml -n  openshift-config
```

## Edit the cluster project config

```bash
oc edit project.config.openshift.io/cluster
```

- Add the following spec
```yaml
spec:
  projectRequestTemplate:
    name: project-request
```

## Post configuration tests

- create a new project and check if resourcequota and limitrange are limited as defined
