# Deploy the devops tools custom helm repo

__Note: we need to think on how breaking the dependency on the devops tools team to manage the deployment of an internal nexus and get their images synchronized__

## Plug the external helm repo
- create a `devopstools.yaml` file based on the following content
```bash
apiVersion: helm.openshift.io/v1beta1
kind: HelmChartRepository
metadata:
  name: devopstools
spec:
  connectionConfig:
    url: 
    name: anyname
```

- apply it
```bash
oc apply -f devopstools.yaml
```
## Add the required privileges to the devops tools team to deploy images in the registry

TODO

## Post installation tasks

- On the web UI, switch to the developer view and click on "Add" from the catalog. Ensure any devopstools chart is available.

