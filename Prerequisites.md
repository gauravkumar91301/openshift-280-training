- [ ] Create Redhat Account 

[Click Here to create Redhat Account ](https://console.redhat.com/ )

- [ ] Download the installer from below url

[Click Here to download the installer](https://console.redhat.com/openshift/install/azure/installer-provisioned )

- [ ] Download pull secret to pull the image

[Click Here to download pull secret ](https://console.redhat.com/openshift/install/azure/installer-provisioned )

- [ ] Create a VM
- [ ] Configure AWS cli if Installing on AWS cloud

[Click here to download awscli](https://aws.amazon.com/cli/)


- [ ] Configure AWS cli if Installing on Azure cloud

[Click here to download azure cli](https://docs.microsoft.com/en-us/cli/azure/)



