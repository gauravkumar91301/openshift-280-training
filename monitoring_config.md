# Configure the monitoring

## Configure the alertmanager

- create a dedicated MS teams channel in our ` - openshift admin` Team and note its email address

- with your cluster-admin account go to the Administration / Cluster Settings / Global Configuration / Alertmanager page

- edit the existing `Default` 
```
receiver type: Email
to address : <CHANGE ME WITH THE EMAIL ADDRESS OF YOUR CREATED TEAM CHANNEL CREATED EARLIER>
From address: 
Smtp smarthost: smtp.office365.com:587
SMTP hello: localhost
Auth Username: 
Auth Password: <CHANGE ME WITH THE MATCHING PASSWORD IN AZURE KEYVAULT>
ensure TLS required is checked
```
- Save it

- delete the alertmanager pods
```bash
oc delete po -l app=alertmanager -n openshift-monitoring
```

## Post Alertmanager config

To test if the alertmanager will send alert properly to the team channel you could just shutdown a node and wait.

## Configure the PVC that will be used for the prometheus DB

- Start editing the cluster-monitoring-config ConfigMap.

```bash
oc -n openshift-monitoring edit configmap cluster-monitoring-config
```

- Put your configuration under data/config.yaml as key-value pair

```yaml
data:
  config.yaml: |+
alertmanagerMain:
  nodeSelector:
    node-role.kubernetes.io/infra: ""
  tolerations:
  - effect: NoSchedule 
    key: node-role.kubernetes.io/infra 
    operator: Exists 
prometheusK8s:
  nodeSelector:
    node-role.kubernetes.io/infra: ""
  tolerations:
  - effect: NoSchedule
    key: node-role.kubernetes.io/infra
    operator: Exists
  retention: 15d
  volumeClaimTemplate:
    spec:
      storageClassName: managed-premium
      volumeMode: Filesystem
      resources:
        requests:
          storage: 100Gi
prometheusOperator:
  nodeSelector:
    node-role.kubernetes.io/infra: ""
  tolerations:
  - effect: NoSchedule
    key: node-role.kubernetes.io/infra
    operator: Exists
grafana:
  nodeSelector:
    node-role.kubernetes.io/infra: ""
  tolerations:
  - effect: NoSchedule
    key: node-role.kubernetes.io/infra
    operator: Exists
k8sPrometheusAdapter:
  nodeSelector:
    node-role.kubernetes.io/infra: ""
  tolerations:
  - effect: NoSchedule
    key: node-role.kubernetes.io/infra
    operator: Exists
kubeStateMetrics:
  nodeSelector:
    node-role.kubernetes.io/infra: ""
  tolerations:
  - effect: NoExecute
    key: infra
    value: reserved
telemeterClient:
  nodeSelector:
    node-role.kubernetes.io/infra: ""
  tolerations:
  - effect: NoExecute
    key: infra
    value: reserved
openshiftStateMetrics:
  nodeSelector:
    node-role.kubernetes.io/infra: ""
  tolerations:
  - effect: NoExecute
    key: infra
    value: reserved
thanosQuerier:
  nodeSelector:
    node-role.kubernetes.io/infra: ""
  tolerations:
  - effect: NoExecute
    key: infra
    value: reserved

```

- monitor if the pvc are created:
```bash
watch oc get pvc
```
